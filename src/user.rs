pub mod permissions;
pub mod settings;

use std::sync::Arc;

use super::lists::Lists;
use permissions::Permission;
use regex::Regex;
use serde::{
    de::{Unexpected, Visitor},
    Deserialize, Serialize,
};
use settings::Settings;

/// A user of the instance
#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    /// ActivityPub Actor ID
    actor_id: ActorID,
    /// The user's username
    username: String,
    /// The users' anime/manga lists
    lists: Lists,
    /// The URL of the user's profile picture
    pfp: Option<String>,
    /// The URL of the user's banner image
    banner: Option<String>,
    /// The Actor IDs of the users this user follows
    following: Vec<Arc<str>>,
    /// The Actor IDs of the users that follow this user
    followers: Vec<Arc<str>>,
    /// The user's settings
    settings: Settings,
    /// The user's permissions
    permissions: Vec<Permission>,
}

/// An ActivityPub Actor ID for a NyanTrack instance. Format is
/// "{instance_url}/user/{user_id}". This does not represent Actor IDs
/// for other fediverse software, such as Mastodon, since these may have any
/// arbitrary format.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ActorID {
    instance_url: Arc<str>,
    user_id: u64,
}

impl ActorID {
    pub fn new(instance_url: &str, user_id: u64) -> Self {
        Self {
            instance_url: instance_url.into(),
            user_id,
        }
    }

    pub fn to_string(&self) -> String {
        self.into()
    }
}

impl From<ActorID> for String {
    fn from(value: ActorID) -> Self {
        format!("{}/user/{}", value.instance_url, value.user_id)
    }
}

impl From<&ActorID> for String {
    fn from(value: &ActorID) -> Self {
        Self::from(value.clone())
    }
}

impl Serialize for ActorID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.collect_str(&self.to_string())
    }
}

struct ActorIDVisitor;
static USERID_EXPECTED: &str = "an integer between 0 and 2^64";
static EXPECTED: &str = "a URL of the form {instance_url}/user/{user_id}, where `user_id` is an integer between 0 and 2^64";

impl<'de> Visitor<'de> for ActorIDVisitor {
    type Value = ActorID;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str(EXPECTED)
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        // Matches only a url in the correct ActorID format, and captures the
        // instance url and user ID
        let actorid_re =
            Regex::new(r"^(?:((?:https?://)?(?:[^:/$]+)(?::(?:\d+))?)(?:/user/(\d+)))$")
                .expect("Hardcoded valid regex");

        // Parse instance URL and user ID out of the input str
        let Some((_, [instance_url, user_id])) = actorid_re.captures(v).map(|c| c.extract()) else {
            return Err(E::invalid_value(Unexpected::Str(v), &EXPECTED));
        };
        let Ok(user_id) = user_id.parse::<u64>() else {
            return Err(E::invalid_type(
                Unexpected::Other(user_id),
                &USERID_EXPECTED,
            ));
        };

        Ok(Self::Value {
            instance_url: instance_url.into(),
            user_id,
        })
    }

    fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        self.visit_str(&v)
    }
}

impl<'de> Deserialize<'de> for ActorID {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_any(ActorIDVisitor)
    }
}
