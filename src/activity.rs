use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum ActivityPrivacy {
    /// All users can see this activity.
    Public,
    /// Only users who follow this user or those within the same instance who
    /// have the ViewPrivateActivity permission can see this activity.
    FollowersOnly,
    /// Only users who both follow and are followed by this user, or those
    /// within the same instance who have the ViewPrivateActivity permission,
    /// can see this activity.
    MutualsOnly,
    /// No one except users within the same instance who have the
    /// ViewPrivateActivity permission can see this activity.
    Private,
}
