use serde::{Deserialize, Serialize};

use crate::{activity::ActivityPrivacy, tags::Tag};

#[derive(Debug, Serialize, Deserialize)]
pub struct Settings {
    /// If true, then other users may follow this user.
    allow_followers: bool,
    /// The default privacy level for this user's activities
    activity_privacy: ActivityPrivacy,
    /// The list of tags this user filters. Entries containing tags in this list
    /// will not appear to the user.
    filtered_tags: Vec<Tag>,
    /// Which types of activity posts are disabled for this user
    disabled_activities: DisabledActivities,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct DisabledActivities {
    /// If true, new activity will not be created when this user watches an
    /// episode or reads a chapter
    watched_read: bool,
    /// If true, new activity will not be created when this user rewatches an
    /// episode or rereads a chapter.
    re: bool,
    /// If true, new activity will not be created when this user completes an
    /// entry.
    completed: bool,
    /// If true, new activity will not be created when this user adds an entry
    /// to their plan to watch/read list.
    planning: bool,
    /// If true, new activity will not be created when this user pauses an entry.
    paused: bool,
    /// If true, new activity will not be created when this user drops an entry.
    dropped: bool,
}
