use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Permission {
    /// Submit changes to local entries for consideration by instance admins.
    ProposeChanges,
    /// Accept or reject change submissions to local entries, and if accepted
    /// forward them to federated instances. Implies ViewChanges.
    ApproveChanges,
    /// See the change submissions that are queued on the instance.
    ViewChanges,
    /// See what permissions other users have.
    ViewUserPermissions,
    /// Change the permissions of other users.
    ModifyUserPermissions,
    /// Delete activity created by other users.
    DeleteUserActivity,
    /// See activity from other users which they have set to private.
    ViewPrivateActivity,
}
