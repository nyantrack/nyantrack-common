use language_tags::LanguageTag;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum CharacterName {
    Native(String),
    Romaji(String),
    Localized { lang: LanguageTag, name: String },
    Nickname(String),
    Other(String),
}
