#![allow(clippy::upper_case_acronyms)]

use std::sync::Arc;

use language_tags::LanguageTag;
use serde::{Deserialize, Serialize};

use super::{character::Character, staff::Staff, studio::Studio};

#[derive(Debug, Serialize, Deserialize)]
pub enum Entry {
    Anime {
        /// The numeric ID of the entry. Must be unique within the set of
        /// entries
        id: u64,
        /// The title(s) of the anime
        titles: Titles,
        /// The format of the anime
        anime_type: AnimeType,
        /// The total number of episodes that will air
        total_episodes: Option<u32>,
        /// The number of episodes that have aired
        aired_episodes: u32,
        /// The description/summary of the anime
        description: String,
        /// What type of source material the anime is based on
        source: Source,
        /// List of characters in the anime
        characters: Vec<Character>,
        /// List of opening theme songs
        openings: Vec<EntrySong>,
        /// List of ending theme songs
        endings: Vec<EntrySong>,
        /// List of staff who worked on the anime
        staff: Vec<Staff>,
        /// List of studios which worked on the anime
        studios: Vec<Studio>,
        /// List of related links, e.g. licensed streaming services, youtube
        /// channels, official websites, etc.
        links: Vec<Link>,
        /// The YouTube ID of the anime's PV
        pv_yt_id: String,
    },
    Manga {
        /// The numeric ID of the entry. Must be unique within the set of
        /// entries
        id: u64,
        titles: Titles,
        total_chapters: Option<u32>,
        published_chapters: u32,
        description: String,
        characters: Vec<Character>,
        staff: Vec<Staff>,
        links: Vec<Link>,
    },
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Titles {
    /// The native title in its native form, e.g. 「進撃の巨人」
    native: Option<String>,
    /// The native title transliterated into the latin alphabet, e.g.
    /// "Shinkegi no Kyojin"
    romaji: Option<String>,
    /// The official localized title(s) in other languages, e.g.
    /// "Attack on Titan", "Ataque a los Titanes"
    localized: Vec<(LanguageTag, String)>,
    /// Commonly used shortened form(s) of the title, e.g. "AoT", "SnK"
    abbreviated: Vec<String>,
    /// Any other titles
    other: Vec<String>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum AnimeType {
    TV,
    Movie,
    OVA,
    ONA,
    Short,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Source {
    Manga,
    LightNovel,
    VisualNovel,
    Original,
    Novel,
    VideoGame,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct EntrySong {
    episodes: Vec<u32>,
    title: Arc<str>,
    links: Vec<SongLink>,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum SongLink {
    Spotify(String),
    YouTube(String),
    Tidal(String),
    AppleMusic(String),
    AmazonMusic(String),
    BandCamp(String),
    Other(String),
}

#[derive(Debug, Serialize, Deserialize)]
pub enum Link {
    PrimeVideo(String),
    Netflix(String),
    Crunchyroll(String),
    OfficialSite(String),
    Twitter(String),
    YouTube(String),
    BilibiliTV(String),
    Funimation(String),
    Hulu(String),
    Max(String),
    HiDive(String),
    Wikipedia(String),
    BookWalker(String),
    CONtv(String),
    DisneyPlus(String),
    Instagram(String),
    TikTok(String),
    Other(String),
}
