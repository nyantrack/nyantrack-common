use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use super::{activity::ActivityPrivacy, entry::Entry};

/// User Anime/Manga list
#[derive(Debug, Serialize, Deserialize)]
pub struct Lists {
    /// Contains all of the entries that the user has given some status.
    /// Individual status lists are derived at runtime.
    entries: Vec<UserEntry>,
    /// Any custom lists that the user has created
    custom_lists: HashMap<String, Vec<UserEntry>>,
}

/// An entry plus related user-specific data
#[derive(Debug, Serialize, Deserialize)]
pub struct UserEntry {
    entry: Entry,
    rating: Rating,
    note: String,
    privacy: ActivityPrivacy,
    status: Option<Status>,
    watched: u32,
    rewatches: u32,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Rating {
    Thumbs(ThumbsRating),
    Numeric(u8),
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum ThumbsRating {
    Up,
    Mid,
    Down,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Status {
    Current,
    Re,
    Complete,
    Dropped,
    Paused,
    Planning,
}
