pub mod activity;
pub mod character;
pub mod entry;
pub mod lists;
pub mod name;
pub mod staff;
pub mod studio;
pub mod tags;
pub mod user;

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn serialize_actor_id() {
        use user::ActorID;

        let actor_id = ActorID::new("https://example.com", 12345);
        let serialized = serde_json::to_string(&actor_id).unwrap();
        assert_eq!(serialized, "\"https://example.com/user/12345\"".to_owned())
    }

    #[test]
    fn deserialize_actor_id() {
        use user::ActorID;

        let actor_id_json = "\"https://example.com/user/12345\"";
        let deserialized: ActorID = serde_json::from_str(actor_id_json).unwrap();
        assert_eq!(deserialized, ActorID::new("https://example.com", 12345));

        let actor_id_json = "\"http://example.com/user/12345\"";
        let deserialized: ActorID = serde_json::from_str(actor_id_json).unwrap();
        assert_eq!(deserialized, ActorID::new("http://example.com", 12345));

        let actor_id_json = "\"https://example.com:8080/user/12345\"";
        let deserialized: ActorID = serde_json::from_str(actor_id_json).unwrap();
        assert_eq!(deserialized, ActorID::new("https://example.com:8080", 12345));

        let actor_id_json = "\"example.com/user/12345\"";
        let deserialized: ActorID = serde_json::from_str(actor_id_json).unwrap();
        assert_eq!(deserialized, ActorID::new("example.com", 12345));
    }
}
