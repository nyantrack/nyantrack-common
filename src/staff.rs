use serde::{Deserialize, Serialize};

use super::entry::Entry;

#[derive(Debug, Serialize, Deserialize)]
pub struct Staff {
    /// The numeric ID of the staff-person. Must be unique within the set of all
    /// staff
    id: u64,
    /// The name(s) of the staff-person (don't assume falsehood 1)
    names: Vec<String>,
    /// List of the entries that they have been part of the staff for and their
    /// roles in those entries.
    works: Vec<(Entry, String)>,
}
