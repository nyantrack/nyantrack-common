use std::sync::Arc;

use serde::{Deserialize, Serialize};

/// Specifies an attribute or genre that is applicable to an entry
#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Tag {
    /// The name of the tag
    tag: Arc<str>,
    /// Other tags that have the same meaning and should also come up in seaches
    /// for this tag
    synonyms: Vec<Tag>,
}
