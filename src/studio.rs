use std::sync::Arc;

use serde::{Deserialize, Serialize};

use super::entry::Entry;

#[derive(Debug, Serialize, Deserialize)]
pub struct Studio {
    /// The numeric ID of the studio. Must be unique within the set of studios.
    id: u64,
    /// The name of the studio
    name: Arc<str>,
    /// List of the entries that the studio has worked on.
    works: Vec<Entry>,
}
