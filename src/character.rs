use serde::{Deserialize, Serialize};

use super::{entry::Entry, name::CharacterName, staff::Staff};

#[derive(Debug, Serialize, Deserialize)]
pub struct Character {
    /// The numeric ID of the character. Must be unique within the set of
    /// characters
    id: u64,
    /// The list of names for the character
    names: Vec<CharacterName>,
    /// A description of the character
    description: String,
    /// List of the VAs who have voiced this character and in what anime
    voice_actors: Vec<(Entry, Staff)>,
    /// List of all entries the character has appeared in
    appearances: Vec<Entry>,
}
